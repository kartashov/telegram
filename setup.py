from setuptools import setup, find_packages


setup(
    version='1.0',
    name='telegram',
    description='Telegram bots',
    author='Kirill Kartashov',
    author_email='kirill@kartashov.kz',
    packages=find_packages(),
)
