from django.conf import settings


# Required settings
BOT_TOKEN = getattr(settings, 'BOT_TOKEN', None)
if not BOT_TOKEN:
    raise NotImplementedError('Bot token is not set. Use setting BOT_TOKEN')