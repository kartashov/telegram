from django.conf import settings
from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from telegram.models import User, Message, Chat, Query, DelayedMessage


class MessageAdmin(admin.ModelAdmin):
    list_display = ('telegram_id', 'chat', 'sender', 'text_short', 'outgoing', 'date', 'download')
    list_filter = ('outgoing',)
    actions = ('forward_to_admin_group', )

    def get_queryset(self, request):
        return super(MessageAdmin, self).get_queryset(request).select_related('chat', 'sender')

    def text_short(self, obj):
        if obj.media:
            return '--[[%s]]--' % obj.media.__class__.__name__.upper()
        elif len(obj.text) < 48:
            return obj.text
        return obj.text[:45] + '...'

    def download(self, obj):
        if hasattr(obj.media, 'file_id'):
            return format_html(u'<a href="{0}" download>Скачать</a> | <a href="{0}" target="_blank">Открыть</a>',
                               reverse('telegram:media', kwargs={'file_id': obj.media.file_id}))
        return ''

    def forward_to_admin_group(self, request, queryset):
        for mess in queryset:
            mess.forward(chat_id=settings.BOT_ADMIN_GROUP)
        self.message_user(request, u'Сообщения пересланы')
    forward_to_admin_group.short_description = u'Переслать сообщения в админ-группу'


admin.site.register(User)
admin.site.register(Chat)
admin.site.register(Message, MessageAdmin)
admin.site.register(DelayedMessage, list_display=('chat', 'datetime', 'sent'))
admin.site.register(Query, list_display=('telegram_id', 'sender', 'query'))