import logging
from django.conf import settings
import requests
import json

logger = logging.getLogger(__name__)


class Bot:
    """
    Пока простенький api для работы с ботами Telegram.
    """
    token = settings.BOT_TOKEN
    url = 'https://api.telegram.org/bot%s/' % token

    def __post(self, method_name, files=None, **params):
        params = params or {}
        files = files or {}
        response = requests.post(self.url + method_name, params, files=files)
        result = json.loads(response.content)
        if result['ok']:
            return result['result']

    def get_me(self):
        return self.__post('getME')

    def send_message(self, chat_id, text, **kwargs):
        kwargs['chat_id'] = chat_id
        kwargs['text'] = text
        return self.__post('sendMessage', **kwargs)

    def send_file(self, chat_id, file, media_type, **kwargs):
        kwargs['chat_id'] = chat_id
        files = None
        if type(file) in (str, unicode):
            kwargs[media_type] = file
        else:
            files = {media_type: file}
        return self.__post('send' + media_type.upper(), files=files, **kwargs)

    def send_document(self, chat_id, document, **kwargs):
        return self.send_file(chat_id, document, 'document', **kwargs)

    def send_photo(self, chat_id, photo, **kwargs):
        return self.send_file(chat_id, photo, 'photo', **kwargs)

    def send_voice(self, chat_id, voice, **kwargs):
        return self.send_file(chat_id, voice, 'voice', **kwargs)

    def send_sticker(self, chat_id, sticker, **kwargs):
        return self.send_file(chat_id, sticker, 'sticker', **kwargs)

    def send_video(self, chat_id, video, **kwargs):
        return self.send_file(chat_id, video, 'video', **kwargs)

    def send_location(self, chat_id, latitude, longitude, **kwargs):
        kwargs['chat_id'] = chat_id
        kwargs['latitude'] = latitude
        kwargs['longitude'] = longitude
        return self.__post('sendLocation', **kwargs)

    def send_chat_action(self, chat_id, action, **kwargs):
        kwargs['chat_id'] = chat_id
        kwargs['action'] = action
        return self.__post('sendChatAction', **kwargs)

    def get_updates(self, **kwargs):
        return self.__post('getUpdates', **kwargs)

    def set_webhook(self, url, certificate=None):
        kwargs = {'url': url}
        if certificate:
            kwargs['certificate'] = certificate
        return self.__post('setWebhook', **kwargs)

    def __getattr__(self, method_name):
        method_name = ''.join(method_name.split('_'))

        def get(self, **kwargs):
            return self.__post(method_name, **kwargs)
        return get.__get__(self)
