import json


class Control:

    def json(self):
        return {}

    def dump(self):
        return json.dumps(self.json())


class Keyboard(Control):

    def __init__(self, buttons, resize=True):
        self.buttons = buttons
        self.resize = resize

    def json(self):
        buttons = []
        for row in self.buttons:
            new_row = []
            for btn in row:
                if isinstance(btn, Control):
                    btn = btn.json()
                new_row.append(btn)
            buttons.append(new_row)

        return {'keyboard': buttons, 'resize_keyboard': self.resize}


class KeyboardButton(Control):

    def __init__(self, text):
        self.text = text

    def json(self):
        return self.text


class RequestContactButton(KeyboardButton):

    def json(self):
        return {'text': self.text, 'request_contact': True}


class RequestLocationButton(KeyboardButton):

    def json(self):
        return {'text': self.text, 'request_location': True}


class InlineKeyboard(Control):

    def __init__(self, buttons):
        self.buttons = buttons

    def json(self):
        buttons = []
        for row in self.buttons:
            new_row = []
            for btn in row:
                new_row.append(btn.json())
            buttons.append(new_row)

        return {'inline_keyboard': buttons}


class InlineKeyboardButton(Control):

    def __init__(self, text, url=None, callback_data=None):
        assert len([a for a in [url, callback_data] if a]) == 1

        self.text = text
        self.url = url
        self.callback_data = callback_data

    def json(self):
        data = {'text': self.text}

        if self.url:
            data['url'] = self.url
        elif self.callback_data:
            data['callback_data'] = self.callback_data

        return data
