class Activity:
    """
    Отдельный экран активности пользователя.
    """
    def __init__(self, data=None, db_entry=None):
        self.data = data or {}
        self.db_entry = db_entry

    def open(self):
        """Метод должен срабатывать, когда пользователь открывает экран"""
        pass

    def process(self, message, *args, **kwargs):
        """Метод должен срабатывать при входящих сообщениях на открытом экране"""
        raise NotImplementedError()

    def close(self):
        if self.db_entry:
            self.db_entry.delete()
