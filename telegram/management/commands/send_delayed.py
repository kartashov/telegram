from datetime import datetime
import logging
from django.core.management.base import BaseCommand
from telegram.models import DelayedMessage


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    Отправляем очередь отложенных сообщений.
    """
    def handle(self, *args, **options):
        messages = DelayedMessage.objects.filter(sent=False, datetime__lte=datetime.now())
        ids = [m.id for m in messages]
        messages.update(sent=True)
        messages = DelayedMessage.objects.filter(id__in=ids)

        for message in messages:
            message.send()
