import logging
import time

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import autoreload

from telegram.bot import Bot
from telegram.models import Update
from telegram.signals import coming_update


logger = logging.getLogger(__name__)


def _run_with_autoreload(*args, **kwargs):
    bot = Bot()
    while True:
        try:
            existing_updates = Update.objects.filter(processed=True).order_by('-telegram_id')
            offset = getattr(settings, 'LAST_UPDATE_ID', 0)
            if existing_updates.exists():
                offset = existing_updates[0].telegram_id + 1
            updates = bot.get_updates(offset=offset)
            for update_data in updates:
                coming_update.send(sender=None, update_data=update_data)
            time.sleep(0.2)
        except KeyboardInterrupt:
            break


class Command(BaseCommand):
    """
    Получаем обновления от Telegram.
    Команда для тестовой среды. Необходимо запустить в фоне для работы бота.
    """
    def handle(self, *args, **options):
        autoreload.run_with_reloader(_run_with_autoreload)
