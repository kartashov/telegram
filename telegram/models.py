import json
import logging

from datetime import datetime

from django.db import models
from django.utils.functional import cached_property

from telegram.bot import Bot
from telegram.signals import message_sent
from telegram.types import Media


bot = Bot()
logger = logging.getLogger(__name__)


class User(models.Model):

    telegram_id = models.BigIntegerField()
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, blank=True)
    username = models.CharField(max_length=50, blank=True)

    @cached_property
    def private_chat(self):
        return Chat.objects.filter(telegram_id=self.telegram_id).first()

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'.strip()

    def __str__(self):
        return self.get_full_name() or self.username or str(self.telegram_id)


class Chat(models.Model):

    TYPES = ('private', 'group', 'supergroup', 'channel')
    PRIVATE, GROUP, SUPER_GROUP, CHANNEL = TYPES
    TYPES = [(t, t) for t in TYPES]

    telegram_id = models.BigIntegerField()
    type = models.CharField(max_length=32, choices=TYPES)
    title = models.CharField(max_length=255, blank=True)
    username = models.CharField(max_length=255, blank=True)
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)

    @cached_property
    def user(self):
        if self.type != 'private':
            return None
        return User.objects.filter(telegram_id=self.telegram_id).first()

    def is_group(self):
        return self.type in (self.GROUP, self.SUPER_GROUP)

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'.strip()

    def __send_message(self, message_type, *args, **kwargs):
        force_reply = kwargs.pop('force_reply', False)
        reply_markup = kwargs.get('reply_markup')
        if not reply_markup and force_reply:
            kwargs['reply_markup'] = json.dumps({'force_reply': True})

        send_function = getattr(bot, 'send_%s' % message_type)
        message_data = send_function(*args, **kwargs)
        # FIXME временное решение по отслеживанию того, что бот был отключен
        if message_data:
            logger.info('Saving outgoing message #%s' % message_data['message_id'])
            Message.objects.get_or_create_from_json(message_data, outgoing=True)
            logger.info('Message #%s saved.' % message_data['message_id'])
            message_sent.send(self.__class__, sent=True, chat=self)
        else:
            message_sent.send(self.__class__, sent=False, chat=self)
        self.save()
        return message_data

    def send_message(self, text, **kwargs):
        return self.__send_message('message', self.telegram_id, text, **kwargs)

    def send_photo(self, photo, caption='', **kwargs):
        return self.__send_message('photo', self.telegram_id, photo, caption=caption, **kwargs)

    def send_voice(self, voice, **kwargs):
        return self.__send_message('voice', self.telegram_id, voice, **kwargs)

    def send_document(self, document, **kwargs):
        return self.__send_message('document', self.telegram_id, document, **kwargs)

    def send_sticker(self, sticker, **kwargs):
        return self.__send_message('sticker', self.telegram_id, sticker, **kwargs)

    def send_location(self, latitude, longitude, **kwargs):
        return self.__send_message('location', self.telegram_id, latitude, longitude, **kwargs)

    def send_action(self, action, **kwargs):
        bot.send_chat_action(self.telegram_id, action, **kwargs)

    def __str__(self):
        return self.title or self.get_full_name() or self.username or str(self.telegram_id)


class MessageManager(models.Manager):

    def get_or_create_from_json(self, data, **kwargs):
        # Получаем пользователя/группу Телеграм и соответствующий чат.
        sender_data = data['from']
        sender_id = sender_data['id']
        default_sender_data = {
            'first_name': sender_data.get('first_name', ''),
            'last_name': sender_data.get('last_name', ''),
            'username': sender_data.get('username', ''),
        }
        sender, created = User.objects.get_or_create(
            telegram_id=sender_id,
            defaults=default_sender_data,
        )

        # TODO лучше изменять данные в БД об имени, фамилии и т.д.

        chat_data = data['chat']
        chat_id = chat_data['id']
        del chat_data['id']
        # Пришлось указать конкретные поля, т.к. Телеграм постоянно что-то добавляет
        default_chat_data = {
            'type': chat_data.get('type', ''),
            'title': chat_data.get('title', ''),
            'username': chat_data.get('username', ''),
            'first_name': chat_data.get('first_name', ''),
            'last_name': chat_data.get('last_name', ''),
        }
        chat, created = Chat.objects.get_or_create(telegram_id=chat_id, defaults=default_chat_data)

        # Сохраняем сообщение.
        text = data.get('text', '').strip()
        date = datetime.fromtimestamp(data['date'])
        return self.get_or_create(
            telegram_id=data['message_id'],
            sender=sender,
            chat=chat,
            date=date,
            defaults={'text': text, 'raw': json.dumps(data)},
            **kwargs,
        )


class Message(models.Model):

    MEDIA_TYPES = ('audio', 'document', 'sticker', 'photo', 'video', 'voice', 'contact', 'location')

    telegram_id = models.BigIntegerField()
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()
    chat = models.ForeignKey(Chat, null=True, on_delete=models.CASCADE)
    text = models.TextField(blank=True)
    outgoing = models.BooleanField(default=False)

    raw = models.TextField(blank=True)

    objects = MessageManager()

    def __media_type(self, data):
        for t in self.MEDIA_TYPES:
            if t in data:
                return t
        return None

    @property
    def media(self):
        if hasattr(self, '_media_cached'):
            return self._media_cached
        if not self.raw:
            return None
        data = json.loads(self.raw)
        media_type = self.__media_type(data)
        if 'photo' in data:
            # Сохраняем только последнее, самое большое фото
            self._media_cached = Media.new('PhotoSize', data['photo'][-1])
        elif media_type:
            self._media_cached = Media.new(media_type.capitalize(), data[media_type])
        else:
            self._media_cached = None
        return self._media_cached

    def forward(self, chat=None, **kwargs):
        chat_id = chat.telegram_id if chat else kwargs.get('chat_id')
        bot.forward_message(
            chat_id=chat_id,
            from_chat_id=self.chat.telegram_id,
            message_id=self.telegram_id,
        )


class DelayedMessage(models.Model):
    """
    Отложенное сообщение.
    """
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    text = models.TextField()
    params = models.TextField()
    datetime = models.DateTimeField()
    sent = models.BooleanField(default=False)

    def send(self):
        kwargs = json.loads(self.params)
        self.chat.send_message(self.text, **kwargs)
        self.sent = True
        self.save()


class Query(models.Model):

    telegram_id = models.CharField(max_length=128)
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    query = models.TextField(blank=True)


class Update(models.Model):

    telegram_id = models.BigIntegerField()
    message = models.OneToOneField(
        to=Message,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    processed = models.BooleanField(default=False)
