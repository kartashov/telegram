import json


class Media:

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def serialize(self):
        return json.dumps({self.__class__.__name__: self.__dict__})

    @staticmethod
    def new(type, data):
        klass = globals()[type]
        return klass(**data)

    @staticmethod
    def restore(raw):
        raw_data = json.load(raw)
        type, data = list(raw_data.items())[0]
        return Media.new(data, type)


class BaseFile(Media):
    pass


class PhotoSize(BaseFile):
    pass


class Audio(BaseFile):
    pass


class Document(BaseFile):
    pass


class Sticker(BaseFile):
    pass


class Video(BaseFile):
    pass


class Voice(BaseFile):
    pass


class Contact(Media):
    pass


class Location(Media):
    pass
