from django.urls import re_path
from django.views.decorators.csrf import csrf_exempt
from telegram.views import MediaView, UserAvatarView, WebhookView

urlpatterns = [
    re_path(r'^webhook/(?P<token>.+)/$', csrf_exempt(WebhookView.as_view()), name='webhook'),
    re_path(r'^avatar/(?P<user_id>.+)/$', UserAvatarView.as_view(), name='avatar'),
    re_path(r'^media/(?P<file_id>.+)/$', MediaView.as_view(), name='media'),
]
