import json
import logging
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponseRedirect, Http404, HttpResponse
from django.views.generic.base import View
from django.urls import reverse
from telegram.bot import Bot
from telegram.signals import coming_update

bot = Bot()
logger = logging.getLogger(__name__)


class WebhookView(View):
    """
    Обработчик входящих сообщений.
    """
    def post(self, request, *args, **kwargs):
        if kwargs['token'] != settings.BOT_TOKEN:
            raise PermissionDenied()
        logger.debug('Telegram incoming message: %s', request.body)
        data = json.loads(request.body)
        coming_update.send(self.__class__, update_data=data)
        return HttpResponse(json.dumps(data), content_type='json')


class UserAvatarView(View):
    """
    Ссылка для просмотра Аватара пользователя.
    """
    def get(self, request, user_id, *args, **kwargs):
        try:
            photos = bot.get_user_profile_photos(user_id=user_id, limit=1)['photos']
            file_id = photos[-1][-1]['file_id']
            return HttpResponseRedirect(reverse('telegram:media', kwargs={'file_id': file_id}))
        except Exception:
            raise Http404


class MediaView(View):
    """
    Ссылка для просмотра медиа-файла.
    """
    def get(self, request, file_id, *args, **kwargs):
        file = bot.get_file(file_id=file_id)
        return HttpResponseRedirect('https://api.telegram.org/file/bot%s/%s' % (bot.token, file['file_path']))
